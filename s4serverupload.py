# s4-drive
# authors: Ondrej Sika, http://ondrejsika.com, dev@ondrejsika.com


DATABASE_NAME_DEFAULT = "s4server"
TMP_DIR_DEFAULT = "/tmp/s4server/"


import random
import string
import os
import json

import pymongo
import flask

from werkzeug import secure_filename


DATABASE_NAME = os.environ.get("S4_SERVER_DATABASE_NAME", DATABASE_NAME_DEFAULT)
TMP_DIR = os.environ.get("S4_SERVER_TMP_DIR", TMP_DIR_DEFAULT)


def getrandomhash(length=40, chars=string.ascii_lowercase+string.digits):
    return "".join([random.choice(chars) for i in xrange(length)])

def upload(addr, file_path):
    cmd = "curl -F file=@%s http://%s:%s/ 2>/dev/null" % (file_path, addr[0], addr[1])
    return os.popen(cmd).read()

def s4serverupload(database_name=DATABASE_NAME, tmp_dir=TMP_DIR):
    db = pymongo.Connection()[database_name]
    drives = list(db.drives.find())

    def get_addr():
        if drives:
            obj = random.choice(drives)
            return (obj["ip"], int(obj["port"]))

    app = flask.Flask(__name__)
    @app.route("/", methods=["GET", "POST"])
    def uploader():
        f = flask.request.files.get("file", None)
        if f:
            addr = get_addr()
            if not addr:
                return "No drives defined", 500
            file_name = secure_filename(f.filename)
            randomhash = getrandomhash(8)
            file_dir = os.path.join(tmp_dir, randomhash)
            os.makedirs(file_dir)
            file_path = os.path.join(file_dir, file_name)
            f.save(file_path)
            url = upload(addr, file_path)
            os.system("rm %s -r"%file_dir)
            file_url = "http://%s%s"%(addr[0], url)
            response = {
                "file_url": file_url,
                "key": getrandomhash(32),
                "delete": getrandomhash(8),
                "name": file_name,
            }
            db.files.insert(response)
            response["url"] = "%s/%s" % (response["key"], file_name)
            response.pop("_id")
            return json.dumps(response)
        return "No file uploaded", 400
    return app

app = s4serverupload()

if __name__ == "__main__":
    app.debug = True
    app.run()